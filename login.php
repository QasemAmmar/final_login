<?php
if (isset($_POST['login'])) {
    $userName=$pass="";

    function check_input($data)
    {
        $data=trim($data);
        $data=htmlspecialchars($data);
        $data=stripslashes($data);
        return $data;
    }
    if ($_SERVER["REQUEST_METHOD"]=="POST") {
        $userName=check_input($_POST['userName']);
        $pass=check_input($_POST['pass']);
    }

    if (file_exists('user/'.$userName.'.xml')) {
        $xml=new SimpleXMLElement('user/'.$userName.'.xml', 0, true);
        if ($pass==$xml->password) {
            session_start();
            $_SESSION['userName']=$userName;
            header('location: user.php?dest='.'files/'.$userName);
            die;
        }
    }
    else {
        echo '<script>alert("UserName Or PassWord Incorrect");</script>';
    }
}


 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>FORM VALIDATION</title>
    <link rel="stylesheet" href="bootstrap.min.css">
  <link rel="stylesheet" href="style.css" type="text/css" />

</head>
<body>
  <div class="container" >
    <div class="welcome">
     <h1>Welcome</h1>
    </div>
    <div class="row" id="rowID">

      <form action="" method="post" >
        <div class="col-md-12" >
            <h2>user name: <input type="text" id="fname" name="userName"/></h2>
            <div id="demo1" class="error">
            </div>
          <h2>password   :  <input type="password" name="pass" id="pass"/></h2>
          <div id="demo2" class="error">
          </div>
        </div>
  <div class="btn">
    <input type="submit" value=" log In" name="login" />
  </div>
      </form>
    </div>
  </div>
</body>
</html>
