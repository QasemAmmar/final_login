<?php
$fname=$userName=$pass=$phone=$email="";
$error=array();
function check_input($data)
{
    $data=trim($data);
    $data=htmlspecialchars($data);
    $data=stripslashes($data);
    return $data;
}
if ($_SERVER["REQUEST_METHOD"]=="POST") {
    $userName=check_input($_POST['userName']);
    $pass=check_input($_POST['pass']);
    $email=check_input($_POST['email']);
    $fname=check_input($_POST['fname']);
    $phone=check_input($_POST['phone']);
}

if (file_exists('user/'.$userName.'.xml')) {
  $error[]='User Name Already Exists';
}
if(count($error)==0){
  $xml=new SimpleXMLElement('<user></user>');
  $xml->addChild('userName',$userName);
  $xml->addChild('password',$pass);
  $xml->addChild('fname',$fname);
  $xml->addChild('phone',$phone);
  $xml->addChild('email',$email);
  $xml->asXML('user/'.$userName.'.xml');
  mkdir("files/".$userName);
  mkdir("files/".$userName.'/'.'Uploaded Files');
  mkdir("files/".$userName.'/'.'Main Files');
  header('location: login.php');
  die;
}

 ?>
