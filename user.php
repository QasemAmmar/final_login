<?php
session_start();
$userName= $_SESSION['userName'];
$GLOBALS['fullPath']='files/'.$userName;

$dest=$_GET['dest'];

  if(isset($dest) && strpos($dest,'files/'.$userName)!==false){
    $folder=array_diff(scandir($dest), array('.', '..'));
    $output='
<table class="table table-bordered">
<tr>
<th>Folder Name</th>
  <th>Delete Folder/File</th>
</tr>
';
    if ($folder >0) {
        foreach ($folder as $name) {
            # code...
            $output .='<tr  >';
if (pathinfo($name, PATHINFO_EXTENSION)){
$output .='<td ><a href="">'.$name.'</a></td>
<td><button type="button" name ="remove" data-name="'.$name.'" class="remove btn btn-danger">Remove File</button></td>';

}
else{
      $output .='<td ><a href="user.php?dest='.$dest.'/'.$name.'">'.$name.'</a></td>
      <td><button type="button" name ="delete" data-name="'.$name.'" class="delete btn btn-danger">Delete Folder</button></td>';

}

$output .='

</tr>
';
        }
    } else {
        $output .='<tr>
  <td colspan="6">No folder Found</td>
  </tr>';
    }
    $output .='</table>  ';

  }
else{
  $output='<table class="table table-bordered">
  <tr>
    <td colspan="6">No folder Found</td>
  </tr>
  </table>';
  header("location: user.php?dest=".'files/'.$userName);
}
 ?>

 <!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
 <meta charset="utf-8">
 <title>User Page</title>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
 <link rel="stylesheet" href="bootstrap.min.css" type="text/css" />
 <link rel="stylesheet" href="style.css" type="text/css" />
 <script src="jquery.min.js"></script>
 <script src="bootstrap.min.js"></script>
</head>
<body>
 <div class="container" style="padding-top:50px;">
   <ul class="nav nav-tabs" role="tablist">
     <li class="nav-item  active" id="makeActive">
       <a class="nav-link active"  href="#user" role="tab" data-toggle="tab"><img src="img\user.png" class="panel-img" title="User"/></a>
     </li>
     <li class="nav-item addFilesModel" >
       <a class="nav-link"  role="tab" data-toggle="tab"><img src="img\new_file.png" class="panel-img" title="Add File"/></a>
     </li>
     <li class="nav-item folderModal">
       <a class="nav-link" role="tab" data-toggle="tab"><img src="img\Folder-Add.png" class="panel-img" title="Add Folder"/></a>
     </li>
     <li class="nav-item uploadModal">
       <a class="nav-link"  role="tab" data-toggle="tab"><img src="img\upload.png" class="panel-img" title="Upload File"/></a>
     </li>
     <li class="nav-item "id="removeActive">
       <a class="nav-link"  role="tab" data-toggle="tab"><img src="img\go-back-icon.png" class="goBack" title="Go Back" style="height:50px;width:50px"/></a>
     </li>
     <li class="nav-item logOut">
       <a class="nav-link"  role="tab" data-toggle="tab"><img src="img\logout.png" class="logout" title="Sign Out" style="height:50px;width:50px"/></a>
     </li>
   </ul>
   <div class="tab-content">
     <div role="tabpanel" class="tab-pane active" id="user">
       <br />
       <div id="folder_table"class="table-responsive">
 <?php echo $output;?>
       </div>
     </div>
     <div role="tabpanel" class="tab-pane" id="file">
         <form action="create.php" method="post">
         <h1> ADD File :</h1>
         <br />
         <h3>File name <input type="text" name="fileName"/> . <select name="extention">
             <option value="txt">text</option>
             <option value="pptx">power point</option>
             <option value="xlsx">exel</option>
             <option value="docx">word</option>
             <option value="pdf">pdf</option>
           </select>
         </h3>
           <input type="hidden" name="type" value="file" />
           <br />

           <input type="submit"  class="btn btn-default" value="Create" />
             </form>
     </div>
     <div role="tabpanel" class="tab-pane" id="folder">

     </div>
     <div role="tabpanel" class="tab-pane" id="upload">
<form action="create.php" method="post" enctype="multipart/form-data">
<label>Please Select a File To Upload
 <input type="file" name="file" style="display:inline"/></label>
 <input type="hidden" name="type" value="upload" />
 <input type="submit" class="btn btn-default" value="Upload" />
</form>
     </div>
   </div>
 </div>
</body>
</html>

<div id="uploadModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Create File</h4>
   </div>
   <div class="modal-body">
     <form action="create.php" method="post" enctype="multipart/form-data">
     <label>Please Select a File To Upload
      <input type="file" name="file" style="display:inline"/></label>
      <input type="hidden" name="type" value="upload" />
        <input type="hidden" name="hidden_folder_name" id="hidden_folder_name" value="<?php echo $dest ?>" />
      <input type="submit" class="btn btn-default" value="Upload" />
     </form>
    </form>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   </div>
  </div>
 </div>
</div>

<div id="folderModel" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Create Folder</h4>
   </div>
   <div class="modal-body" id="file_list">
     <form action="create.php" method="post" autocomplete="off">
       <h1> ADD Folder :</h1>
       <br />
       <h3>Folder name : <input type="text"  name="folderName"/></h3>
       <br />
       <input type="hidden" name="type" value="folder" />
        <input type="hidden" name="hidden_folder_name" id="hidden_folder_name" value="<?php echo $dest ?>" />
       <input type="submit" name="folderSubmit" class="btn btn-default" value="Create"/>
     </form>

   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   </div>
  </div>
 </div>
</div>

<div id="addFilesModel" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">ADD FILE</h4>
   </div>
   <div class="modal-body" id="file_list">
     <form action="create.php" method="post">
     <h1> ADD File :</h1>
     <br />
     <h3>File name <input type="text" name="fileName" style="width:200px"/> . <select name="extention">
         <option value="txt">text</option>
         <option value="pptx">power point</option>
         <option value="xlsx">exel</option>
         <option value="docx">word</option>
         <option value="pdf">pdf</option>
       </select>
     </h3>
       <input type="hidden" name="type" value="file" />
       <input type="hidden" name="hidden_folder_name" value="<?php echo $dest ?>" />
       <br />

       <input type="submit"  class="btn btn-default" value="Create" />
         </form>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   </div>
  </div>
 </div>
</div>

<script>
$(document).ready(function(){


uploadModal
//uploade model
$(document).on('click','.uploadModal',function(){
    $('#uploadModal').modal('show');
})
//create file model
$(document).on('click','.addFilesModel',function(){
    $('#addFilesModel').modal('show');
})

//folder create model
$(document).on('click','.folderModal',function(){
    $('#folderModel').modal('show');

})

//go back
$(document).on('click','.goBack',function(){
  var userName='<?php echo $userName;?>'
    window.location='user.php?dest=files/'+userName;
})

//delete folders
  $(document).on("click", ".delete", function(){
var dir='<?php echo $dest ;?>';
   var action = "delete";
   var folder_name = $(this).data("name");
   var dest=dir+"/"+folder_name;

   if(confirm("Are you sure you want to remove it?"))
   {
    $.ajax({
     url:"action.php",
     method:"POST",
     data:{dest:dest, action:action},
     success:function(data)
      {
window.location = 'user.php?dest='+dir+'';

      }
    });
   }
  });

  //remove file_exists
  $(document).on("click", ".remove", function(){
var dir='<?php echo $dest ;?>';
   var action = "remove";
   var folder_name = $(this).data("name");
   var dest=dir+"/"+folder_name;

   if(confirm("Are you sure you want to remove it?"))
   {
    $.ajax({
     url:"action.php",
     method:"POST",
     data:{dest:dest, action:action},
     success:function(data)
      {
window.location = 'user.php?dest='+dir+'';

      }
    });
   }
  });
  $(document).on('click','.logOut',function(){
window.location = 'index.php';

  })

  });
</script>
